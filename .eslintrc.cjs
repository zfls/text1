module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    "vue/setup-compiler-macros":true
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/vue3-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended", // 新增，必须放在最后面
    'standard'
  ],
  overrides: [
  ],
      // 新增，解析vue文件
    "parser":"vue-eslint-parser",
   parserOptions: {
    ecmaVersion: 'latest',
    parser: "@typescript-eslint/parser",
    // project: ["./tsconfig.json"],
    sourceType: 'module'
    
  },
  plugins: [
    'vue',
    "@typescript-eslint"
  ],
  rules: {
    semi: 'off',
    'quotes':"off",
    'comma-dangle': 'off',
    'vue/multi-word-component-names': 'off',
    '@typescript-eslint-var-requires': 'off',
    'space-before-function-paren': 0,
      "@typescript-eslint/ban-types": [
        "error",
        {
          "extendDefaults": true,
          "types": {
            "{}": false
          }
        }
      ]
  }
}
